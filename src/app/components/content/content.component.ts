import { Component, OnInit } from '@angular/core';
import { StarWarsCharcters } from '../models/IStarWarsCharcaters';
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent implements OnInit {
  public logoRebeldes: string;
  public logoImperio: string;
  public characters: StarWarsCharcters[];
  constructor() {
    this.logoRebeldes = './assets/img/logorebeldes.png',
    this.logoImperio = './assets/img/logoimperio.png',
    this.characters = [
      {
        name: 'Luke Skywalker',
        image: './assets/img/LukeSkywalker.jpg',
        race: 'Human',
        planetBorn: 'Tatooine',
        age: 20,
        jediorSith: {
          wichSide: 'jedi',
        },
      },
      {
        name: 'Dardth Vader',
        image: './assets/img/DardhVader.jpeg',
        race: 'Human',
        planetBorn: 'Tatooine',
        age: 45,
        jediorSith: {
          wichSide: 'sith',
        },
      },
      {
        name: 'Han Solo',
        image: './assets/img/HanSolo.jpg',
        race: 'Human',
        planetBorn: 'Corellia',
        age: 29,
        jediorSith: {
          wichSide: 'none',
        },
      },
      {
        name: 'Yoda',
        image: './assets/img/Yoda.jpg',
        race: 'Unknow',
        planetBorn: 'Unknow',
        age: 900,
        jediorSith: {
          wichSide: 'jedi',
        },
      },
      {
        name: 'Chewbacca',
        image: './assets/img/Chewbacca.jpg',
        race: 'Wookiee',
        planetBorn: 'Kashyyyk',
        age: 190,
        jediorSith: {
          wichSide: 'none',
        },
      },
      {
        name: 'Leia Skywalker',
        image: './assets/img/LeiaSkywalker.jpg',
        race: 'Human',
        planetBorn: 'Alderaan',
        age: 20,
        jediorSith: {
          wichSide: 'jedi',
        },
      },
      {
        name: 'Lord Sidius',
        image: './assets/img/LordSidius.jpg',
        race: 'Human',
        planetBorn: 'Naboo',
        age: 72,
        jediorSith: {
          wichSide: 'sith',
        },
      },
      {
        name: 'Obi Wan',
        image: './assets/img/ObiWanKenobi.jpg',
        race: 'Human',
        planetBorn: 'Stewjon',
        age: 47,
        jediorSith: {
          wichSide: 'jedi',
        },
      },
    ];
  }

  ngOnInit(): void {}
}
