import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public logo: string;
  public logoName: string;
  public homeUrl: string;
  public pageUrl: string;
  public aboutUrl: string;

  constructor() {
    this.logo = './assets/img/StarWarsLogo.png';
    this.logoName = 'StarWars Logo';
    this.homeUrl = 'Home';
    this.pageUrl = 'My Page';
    this.aboutUrl = 'About';
  }

  ngOnInit(): void {
  }

}
