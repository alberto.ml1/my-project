export interface StarWarsCharcters {
    name: string;
    image: string;
    race: string;
    planetBorn: string;
    age: number;
    jediorSith: JediorSith;
}
export interface JediorSith {
    wichSide: 'jedi' | 'sith' | 'none';
}
