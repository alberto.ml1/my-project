import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public copyright: string;
  public moreInformation: string;
  constructor() {
    this.copyright = 'This page is made by @alberto_ml1';
    this.moreInformation = 'https://es.wikipedia.org/wiki/Anexo:Personajes_de_Star_Wars';
  }

  ngOnInit(): void {
  }

}
